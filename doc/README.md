Prerequisites
=============

If you don't have one already, you need a LaTeX distribution (for example, TexLive) and latexmk.
LaTeX can be edited with any text editor, but you might prefer something like TeXStudio or Emacs with the AucTeX mode.

Editing the Paper
=================

The main text is in the file `main.tex`.
After editing this file, you can run `make` in order to generate the PDF version.

Guidelines
----------

*Please* enter text in one sentence per line.
This helps greatly with seeing differences between commits and with resolving merges if necessary.

Add references as individual files in the references subdirectory here.
From a reference manager, you should export each individual reference to BibTeX and set the filename to be the citation handle you want to use.
After adding the bib files there and generating the PDF, use `git add references` to stage the references you've added for the next commit.
