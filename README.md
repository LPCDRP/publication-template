This is a template framework for studies with (semi-)automated analyses integrated with a LaTeX-based manuscript.
To get started, download (do not clone this repository unless you want to develop the template further) a snapshot of this repository, modify it to suit your project, and then commit it as a starting point.

A (slightly modified) PLoS template is included as an example.

See [doc/README.md](doc/README.md) for a description of how to work with the manuscript and standalone reference files.

# Example Project

Run `make` or `make help` in this directory to see available commands.
The manuscript sources are not provided with the article's supplemental materials, so some commands relying on them will not function in this case.

## Analysis Prerequisites

* GNU make
* ...

(It would be better to configure a Singularity or Docker container to run your analyses.
This way, the software environment can be preconfigured and run portably.)
