# Common variable definitions and settings
# Include this file from all sub-makefiles in order to make use of them.

topsrcdir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# Use proper delimiters in awk
AWK = awk -v FS='	' -v OFS='	'

NPROC ?= $(shell nproc)
NPROC := $(NPROC)

VERSION := $(shell git describe --always)


.DELETE_ON_ERROR:
